package solid.demo.application;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ro.pippo.core.Application;
import solid.demo.common.config.CommonConfig;

/**
 * @author  Andrei T.
 */

@Configuration
@Import(CommonConfig.class)
public class CrudConfig {

    @Bean
    public Application application() {
        return new CrudApplication();
    }
}
