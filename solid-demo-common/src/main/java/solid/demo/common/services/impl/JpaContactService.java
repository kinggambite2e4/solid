/*
 * Copyright (C) 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package solid.demo.common.services.impl;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import solid.demo.common.entites.Contact;
import solid.demo.common.repositories.ContactRepository;
import solid.demo.common.services.ContactService;

import java.util.*;

/**
 * @author Andrei T.
 */

@Service
public class JpaContactService implements ContactService {


    @Autowired
    @Setter
    private ContactRepository contactRepository;

    private static Long nextId = 1L;

    private List<Contact> contacts;

    public JpaContactService() {

    }


    @Override
    public List<Contact> getContacts() {
        return Collections.unmodifiableList(contacts);
    }

    @Override
    public Contact getContact(Long id) {
        Optional<Contact> contact = contactRepository.findById(id);
        return contact.orElse(null);
    }

    @Override
    public void delete(Long id) {

        Contact contact = getContact(id);
        if (contact != null)
            contactRepository.delete(contact);

        contacts = contactRepository.findAll();
    }

    @Override
    public Contact save(Contact contact) {
        Contact newContact = contactRepository.save(contact);
        contacts = contactRepository.findAll();
        return newContact;

    }

    public List<Contact> search(String search) {
        contacts = contactRepository.searchByAnyString(search);
        return contacts;

    }

    public void addDefaultContacts() {
        contacts = new ArrayList<>();
        contacts.add(new Contact("Ana", "079562341536", "ana@gmail.com"));
        contacts.add(new Contact("Ion", "069897845662", "ion@mail.md"));
        contacts.add(new Contact("Gigi", "089562565655", "gigi@yahoo.com"));
        contacts = contactRepository.saveAll(contacts);
    }

}
