package solid.demo.common.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;
import solid.demo.common.entites.Contact;

import java.util.List;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {


    @Query("SELECT c FROM contact c WHERE c.name LIKE %?1%  or c.email LIKE %?1% ")
    List<Contact> searchByAnyString(String search);


}
