/*
 * Copyright (C) 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package solid.demo.common.entites;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import java.io.Serializable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;


@Entity(name = "contact")
@Embeddable
@NoArgsConstructor
public class Contact implements Serializable {

    private static final long serialVersionUID = 1L;


    @Id
    @Column(name = "id", unique=true,updatable=false,insertable=false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Setter
    @Getter
    private Long id;
    @Setter
    @Getter
    private String name;

    @Setter
    @Getter
    private String phone;

    @Setter
    @Getter
    private String email;

    @Setter
    @Getter
    @Transient
    private String search;

    public Contact(String name, String phone, String email) {
        this.name = name;
        this.phone = phone;
        this.email = email;
    }


    @Override
    public String toString() {
        return "Contact {" +
                "name='" + name + '\'' +
                ", phone=" + phone +
                ", address='" + email + '\'' +
                '}';
    }


}
